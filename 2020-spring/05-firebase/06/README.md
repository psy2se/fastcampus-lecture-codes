# 06. Firebase 서버에서 정렬과 필터 적용하기

> `showPhotos`에서 정렬과 필터 기능 삭제
```javascript
// 필터 & 정렬 적용
// var filtered = photos.filter(filter);
// filtered.sort(sort);
filtered = photos;
```

<br>

## 서버에서 정렬하여 가져오기
* 문서의 `데이터 읽기 > 데이터 정렬 및 제한` 항목 살펴보기  
* `loadPhotos` 함수 변형해보기

<br>

> `idx` 오름차순 
```javascript
function loadPhotos () {
  db.collection("photos").orderBy('idx').get().then(function (querySnapshot) {
    // ...
  });
}
```
```javascript
function loadPhotos () {
  db.collection("photos").orderBy('idx', 'asc').get().then(function (querySnapshot) {
    // ...
  });
}
```
> `idx` 내림차순 
```javascript
function loadPhotos () {
  db.collection("photos").orderBy('idx', 'desc').get().then(function (querySnapshot) {
    // ...
  });
}
```

<br>

> `likes` 오름차순 
```javascript
function loadPhotos () {
  db.collection("photos").orderBy('likes').get().then(function (querySnapshot) {
    // ...
  });
}
```
```javascript
function loadPhotos () {
  db.collection("photos").orderBy('likes', 'asc').get().then(function (querySnapshot) {
    // ...
  });
}
```
> `likes` 내림차순 
```javascript
function loadPhotos () {
  db.collection("photos").orderBy('likes', 'desc').get().then(function (querySnapshot) {
    // ...
  });
}
```

<br>
<br>

## 서버에서 필터링해서 가져오기
* 문서의 `데이터 읽기 > 단순 쿼리 및 복합 쿼리 실행` 항목 살펴보기  
* `loadPhotos` 함수 변형해보기

> 모든 사진 가져오기
```javascript
function loadPhotos () {
  db.collection("photos").where("idx",  ">", 0).get().then(function (querySnapshot) 
    // ...
  });
}
```

<br>

> `user_id`값으로 필터링하기
```javascript
function loadPhotos () {
  db.collection("photos").where('user_id', '==', 'fastcampus').get().then(function (querySnapshot) 
    // ...
  });
}
```

<br>

> `idx` 포함 여부로 필터링하기
```javascript
function loadPhotos () {
  db.collection("photos").where('idx', 'in', [1587359450484, 1587359520002, 1587359671688]).get().then(function (querySnapshot) {
    // ...
  });
}
```

<br>

> `user_id` 포함 여부로 필터링하기
```javascript
function loadPhotos () {
  db.collection("photos").where('user_id', 'in', ['pixa', 'com-al-mot', 'photo-luv']).get().then(function (querySnapshot) 
    // ...
  });
}
```

<br>
<br>

## 정렬과 필터 조합 한계
현재의 `Firestore`는 다양한 항목에 따라 복합적으로 필터링과 정렬을 수행하는 기능에 한계가 있음
* 대안 : 서버에서 필터링하여 받아온 뒤 로컬에서 정렬

<br>

> `showPhotos`에서 정렬 복원
```javascript
// 필터 & 정렬 적용
// var filtered = photos.filter(filter);
filtered = photos;
filtered.sort(sort);
```

<br>
<br>

## 서버에서 필터링하도록 사이트 변경

> `filterName`변수와 `getFilterParams` 함수 생성
```javascript
var filterName = 'all';

var getFilterParams = {
  all: function () { return ['idx', '>', 0] },
  mine: function () { return ['user_id', '==', my_info.id] },
  like: function () { return ['idx', 'in', my_info.like] },
  follow: function () { return ['user_id', 'in', my_info.follow] }
}
```

<br>

> `loadPhotos` 함수 수정
```javascript
function loadPhotos () {
  db.collection("photos").where(
    getFilterParams[filterName]()[0],
    getFilterParams[filterName]()[1],
    getFilterParams[filterName]()[2]
  ).get().then(function (querySnapshot) {
    // ...
  });
}
```

<br>

> `setFilter` 함수 수정
```javascript
function setFilter(_filter) {
  // ...
  // showPhotos();
  filterName = _filter;
  loadPhotos();
}
```

<br>

***

<br>

## 다음 강좌
* [07. Firebase 상세페이지와 댓글 만들기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/05-firebase/07/README.md)