# Practice 03. 내 정보 수정 & 저장하기 

<a href="https://fastcampus-all.netlify.com/2020-spring/04-javascript/practice-04" target="_blank">이번강 완성본 보기</a>
<br>
<a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/04-javascript/practice-03/practice-03.zip" target="_blank">
  [실습파일 다운로드 받기]
</a>
<br>
<br>

> `내 정보` 탭으로 초기화 

```html
<li class="myinfo on" onclick="setMenu('myinfo')">내 정보</li>
```
```html
<main class="myinfo">
```

<br>

***

<br>

## data의 내 정보대로 내용 채우기
사이트에 들어오면 `#myinfo` 섹션에 내용 채우는 함수 실행

<br>

### `showMyInfo` 함수
```javascript
function showMyInfo () {
  // ...
}
```

### `init` 함수
> 페이지가 모두 로드되면 실행되는 함수
```javascript
function init() {
  showMyInfo();
}
```
```html
<body onload="init()">
```

<br>

### `showMyInfo` 함수 내용부

> 텍스트 항목들 내용 채우기
```javascript
document.querySelector("#myInfoId").innerHTML = my_info.id;
document.querySelector("#myInfoUserName").innerHTML = my_info.user_name;
document.querySelector("#sp-intro").innerHTML = my_info.introduction;
document.querySelector("#ip-intro").value = my_info.introduction;
```

<br>

> `radio` 타입 `input` 체크 설정
```javascript
  document.querySelector("#myinfo input[type=radio][value=" + my_info.as + "]").checked = true;
```

<br>

> `checkbox` 타입 `input` 체크 설정

```javascript
  document.querySelectorAll("#myinfo input[type=checkbox]")
    .forEach(function(checkbox) {
    checkbox.checked = false;
  });
  my_info.interest.forEach(function (interest) {
    document.querySelector(
      "#myinfo input[type=checkbox][value=" + interest + "]"
      ).checked = true;
  });
```

<br>

***

<br>

## 수정 모드 켜고 끄기
* `on` 여부를 `true`, `false` 인자로 넣어줌
* `on`일 시 보기 전용, `!on`일 시 입력 전용 요소들을 감춤
* `on` 여부에 따라 각 `input` 요소들의 `disabled`여부 설정

<br>

### 버튼들에 설정

```javascript
function setEditMyInfo (on) {
  // ...
}
```
```html
<div class="mi-dep non-edit button" onclick="setEditMyInfo(true)">수정</div>
<div class="mi-dep edit button cancel" onclick="setEditMyInfo(false)">취소</div>
```

<br>


### `setEditMyInfo` 함수 내

> 요소들을 포함하는 `div` 요소의 클래스 설정
```javascript
document.querySelector("#myinfo > div").className = on ? "edit" : "non-edit"
```

<br>

> `input`들의 활성화 설정
```javascript
document.querySelectorAll("#myinfo input").forEach(function (input) {
  input.disabled = !on;
})
```

<br>

> 수정하다 취소시 입력창 원상복구
```javascript
showMyInfo();
```

<br>

***

<br>

## 입력한 내용 저장하기

* 각 `input` 요소들의 값 또는 체크 여부에 따라 `my_info` 내용 수정
* 수정 모드 종료  
    * `setEditMyInfo` 사용
* 수정된 `my_info` 내용에 따라 요소들 새로 채우기
    * `showMyInfo` 재실행

<br>

### 확인 버튼에 설정

```javascript
function updateMyInfo () {
  // ...
}
```
```html
<div class="mi-dep edit button" onclick="updateMyInfo()">확인</div>
```

<br>

### `updateMyInfo` 함수

> `my_info` 내용 수정
```javascript
my_info.introduction = document.querySelector("#ip-intro").value;
my_info.as = document.querySelector("#myinfo input[type=radio]:checked").value;
var interests = [];
document.querySelectorAll("#myinfo input[type=checkbox]:checked").forEach(function (checked) {
  interests.push(checked.value);
});
my_info.interest = interests;
```

<br>

> 변경한 내용 반영
```javascript
setEditMyInfo(false);
showMyInfo();
```

<br>

***

<br>

## 다음 강좌
* [04. 썸네일 나타내고 좋아요 토글하기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/practice-04/README.md)